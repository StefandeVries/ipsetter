#!/bin/bash
# It's not pretty but it works

echo "Trying 10.42.0.87 with gateway 10.42.0.1"

ip link set dev eth0 down
ip addr add 10.42.0.87/24 dev eth0
ip link set dev eth0 up
ip route add default via 10.42.0.1

ping -c 3 google.com

if [ $? -eq 0 ]; then
	echo "Succesfully set IP and gateway."
	exit
fi

echo "Trying 192.168.1.80 with gateway 192.168.1.254"

ip link set dev eth0 down
ip addr add 192.168.1.80/24 dev eth0
ip link set dev eth0 up
ip route add default via 192.168.1.254

ping -c 3 google.com
if [ $? -eq 0 ]; then
	echo "Succesfully set IP and gateway."
	exit
fi


